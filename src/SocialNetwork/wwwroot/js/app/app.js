﻿'use strict';

var socialNetworkApp = angular.module('socialNetworkApp', ['ngRoute', 'ngResource', 'ngCookies']);

socialNetworkApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'js/app/templates/home/index.html',
        controller: 'homeController'
    });

    $routeProvider.when('/login', {
        templateUrl: 'js/app/templates/auth/login.html',
        controller: 'loginController'
    });

    $routeProvider.when('/register', {
        templateUrl: 'js/app/templates/auth/register.html',
        controller: 'registerController'
    });

    $routeProvider.when('/timeline', {
        templateUrl: 'js/app/templates/messages/index.html',
        controller: 'messageController'
    });

    $routeProvider.when('/users', {
        templateUrl: 'js/app/templates/users/list.html',
        controller: 'userController'
    });

    $routeProvider.when('/edit-profile', {
        templateUrl: 'js/app/templates/users/edit.html',
        controller: 'editUserController'
    });

    $routeProvider.when('/my-subscriptions', {
        templateUrl: 'js/app/templates/subscriptions/list.html',
        controller: 'subscriptionController'
    });

    $routeProvider.when('/my-messages', {
        templateUrl: 'js/app/templates/messages/edit.html',
        controller: 'editMessageController'
    });

    $routeProvider.when('/messages/from/:username', {
        templateUrl: 'js/app/templates/messages/user.html',
        controller: 'userMessageController'
    });
    
    $routeProvider.when('/my-subscribers', {
        templateUrl: 'js/app/templates/subscribers/list.html',
        controller: 'subscriberController'
    });

    $routeProvider.otherwise({
        controller: 'homeController',
        templateUrl: 'js/app/templates/home/notFound.html',
    })

    $locationProvider.html5Mode(true);
})
.run(function($rootScope, $location) {
    $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
        var path = $location.path();

        if (path !== "/" && path !== "/login" && path != "/register" && !sessionStorage.getItem("authToken")) {
            event.preventDefault();
            window.location.href = "/";
        }
    });
});