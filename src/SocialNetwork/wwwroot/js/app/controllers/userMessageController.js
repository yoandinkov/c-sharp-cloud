﻿'use strict';

socialNetworkApp.controller('userMessageController', function ($scope, $http, $routeParams) {
    $scope.username = $routeParams.username;
    $scope.messages = [];
    $scope.page = 0;
    $scope.count = "5";

    $scope.listMessages = function listMessages() {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/messages/by-user?username='  + $scope.username + '&page' + $scope.page +'&count=' + $scope.count
        }).then(function (result) {
            $scope.messages = result.data;
        }, function (error) {
            showError(error);
        })
    };

    $scope.noMessages = function () {
        if ($scope.messages) {
            return $scope.messages.length === 0;
        }

        return true;
    }

    $scope.isFirstPage = function () {
        return $scope.page == 0;
    }

    $scope.goToPrevPage = function () {
        if ($scope.page > 0) {
            $scope.page -= 1;
            $scope.listMessages();
        }
    }

    $scope.goToNextPage = function () {
        $scope.page += 1;
        $scope.listMessages();
    }

    $scope.isLastPage = function () {
        if ($scope.messages) {
            return $scope.messages.length === 0;
        }

        return true;
    }

    $scope.listMessages();
});