﻿'use strict';

socialNetworkApp.controller('subscriberController', function ($scope, $http) {
    $scope.page = 0;
    $scope.count = "25";

    $scope.listSubscribers = function () {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/subscribers?page=' + $scope.page + '&count=' + $scope.count
        }).then(function (result) {
            $scope.subscribers = result.data;
        }, function (error) {
            console.error(error);
        });
    };

    $scope.block = function (subscriber) {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"],
                "Content-Type": "application/json"
            },
            method: 'PUT',
            url: '/api/subscribers/block',
            data: {
                subscriptionId: subscriber.subscriptionId
            }
        }).then(function (result) {
            subscriber.isBlocked = true;
        }, function (error) {
            console.error(error);
        });
    }

    $scope.unblock = function (subscriber) {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"],
                "Content-Type": "application/json"
            },
            method: 'PUT',
            url: '/api/subscribers/unblock',
            data: {
                subscriptionId: subscriber.subscriptionId
            }
        }).then(function (result) {
            subscriber.isBlocked = false;
        }, function (error) {
            console.error(error);
        });
    }

    $scope.isFirstPage = function () {
        return $scope.page == 0;
    }

    $scope.goToPrevPage = function () {
        if ($scope.page > 0) {
            $scope.page -= 1;
            $scope.listSubscribers();
        }
    }

    $scope.goToNextPage = function () {
        $scope.page += 1;
        $scope.listSubscribers();
    }

    $scope.isLastPage = function () {
        if ($scope.subscribers) {
            return $scope.subscribers.length === 0;
        }

        return true;
    }

    $scope.listSubscribers();
});