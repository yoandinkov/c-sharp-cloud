﻿'use strict';

socialNetworkApp.controller('subscriptionController', function ($scope, $http) {
    $scope.page = 0;
    $scope.count = "25";

    $scope.listSubscriptions = function () {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/subscriptions?page=' + $scope.page + '&count=' + $scope.count
        }).then(function (result) {
            $scope.subscriptions = result.data;
        }, function (error) {
            console.error(error);
        });
    };

    $scope.isFirstPage = function () {
        return $scope.page == 0;
    }

    $scope.goToPrevPage = function () {
        if ($scope.page > 0) {
            $scope.page -= 1;
            $scope.listSubscriptions();
        }
    }

    $scope.goToNextPage = function () {
        $scope.page += 1;
        $scope.listSubscriptions();
    }

    $scope.isLastPage = function () {
        if ($scope.subscriptions) {
            return $scope.subscriptions.length === 0;
        }

        return true;
    }

    $scope.unsubscribe = function (subscription) {
        console.log(subscription)
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"],
                "Content-Type": "application/json"
            },
            data: {
                userId: subscription.authorId
            },
            method: 'DELETE',
            url: '/api/subscriptions'
        }).then(function (result) {
            var index = $scope.subscriptions.indexOf(subscription);
            $scope.subscriptions.splice(index, 1);
        }, function (error) {
            console.error(error);
        });
    }

    $scope.listSubscriptions();
});