﻿'use strict';

socialNetworkApp.controller('editMessageController', function ($scope, $http, $timeout) {
    $scope.content = '';
    $scope.successMessage = '';
    $scope.showSuccess = false;
    $scope.showError = false;
    $scope.errorMessage = '';
    $scope.messages = [];
    $scope.page = 0;
    $scope.count = "5";

    $scope.listMessages = function listMessages() {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/messages/my?page=' + $scope.page + '&count=' + $scope.count
        }).then(function (result) {
            $scope.messages = result.data;
        }, function (error) {
            showError(error);
        })
    };

    $scope.noMessages = function () {
        if ($scope.messages) {
            return $scope.messages.length === 0;
        }

        return true;
    }

    $scope.isFirstPage = function () {
        return $scope.page == 0;
    }

    $scope.goToPrevPage = function () {
        if ($scope.page > 0) {
            $scope.page -= 1;
            $scope.listMessages();
        }
    }

    $scope.goToNextPage = function () {
        $scope.page += 1;
        $scope.listMessages();
    }

    $scope.isLastPage = function () {
        if ($scope.messages) {
            return $scope.messages.length === 0;
        }

        return true;
    }

    $scope.editMessage = function (message) {
        $http({
            method: 'PUT',
            headers: {
                'x-auth': sessionStorage['authToken'],
                "Content-Type": "application/json"
            },
            url: '/api/messages',
            data: {
                id: message.id,
                content: message.content
            }
        }).then(function success() {
            displaySuccess("Updated successfully");
        }, function error(error) {
            console.error(error);
            displayError(error.data);
        })
    }

    $scope.deleteMessage = function (message) {
        $http({
            method: 'DELETE',
            headers: {
                'x-auth': sessionStorage['authToken'],
                "Content-Type": "application/json"
            },
            url: '/api/messages',
            data: {
                messageId: message.id
            }
        }).then(function success() {
            var index = $scope.messages.indexOf(message);
            $scope.messages.splice(index, 1);
        }, function error(error) {
            console.error(error);
            displayError(error.data);
        })
    }

    function displaySuccess(message) {
        $scope.showError = false;
        $scope.successMessage = message;
        $scope.showSuccess = true;

        $timeout(function () {
            $scope.showSuccess = false;
        }, 3000);
    }

    function displayError(message) {
        $scope.showSuccess = false;
        $scope.errorMessage = message;
        $scope.showError = true;
    }

    $scope.listMessages();
});