﻿'use strict';

socialNetworkApp.controller('editUserController', function ($scope, $http, $timeout) {
    $scope.successMessage = '';
    $scope.showSuccess = false;
    $scope.showError = false;
    $scope.errorMessage = '';

    $scope.user = {
        description: '',
        username: '',
        email: ''
    };

    $scope.credentials = {
        oldPassword: '',
        firstPassword: '',
        secondPassword: ''
    }

    $scope.loadUser = function () {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/users/personal-info'
        }).then(function (result) {
            $scope.user = result.data;
        }, function (error) {
            console.error(error);
        });
    };

    $scope.editUser = function () {
        if ($scope.credentials.firstPassword != $scope.credentials.secondPassword) {
            displayError("New password should match.")
            return;
        }

        var credentials = {
            oldPassword: encryptPassword($scope.credentials.oldPassword),
            newPassword: encryptPassword($scope.credentials.firstPassword)
        }

        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'PUT',
            data: {
                description: $scope.user.description,
                oldPassword: credentials.oldPassword,
                newPassword: credentials.newPassword
            },
            url: '/api/users/edit'
        }).then(function (result) {
            displaySuccess("Successfully updated!")
        }, function (error) {
            displayError(error.data);
        });
    };

    function displaySuccess(message) {
        $scope.showError = false;
        $scope.successMessage = message;
        $scope.showSuccess = true;

        $timeout(function () {
            $scope.showSuccess = false;
        }, 3000);
    }

    function displayError(message) {
        $scope.showSuccess = false;
        $scope.errorMessage = message;
        $scope.showError = true;
    }

    function encryptPassword(password) {
        if (!password) {
            return password;
        }

        return CryptoJS.SHA3(password).toString();
    }
});