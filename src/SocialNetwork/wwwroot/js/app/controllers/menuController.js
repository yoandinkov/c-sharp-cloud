﻿'use strict';

socialNetworkApp.controller('menuController', function ($scope, $http) {
    $scope.display = function () {
        return sessionStorage['authToken'];
    }

    $scope.username = sessionStorage['username'];

    $scope.logout = function () {

        $http({
            method: 'PUT',
            headers: {
                'x-auth': sessionStorage['authToken']
            },
            url: '/api/auth/logout'
        }, function success(result) {
        }, function error(error) {
            console.log(error);
        });

        sessionStorage.removeItem('authToken');
        sessionStorage.removeItem('username');
    }
});