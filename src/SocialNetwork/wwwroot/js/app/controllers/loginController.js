﻿'use strict';

socialNetworkApp.controller('loginController', function ($scope, $http) {
    $scope.credentials = {
        username: '',
        password: ''
    };

    $scope.error = {
        visible: false,
        message: ''
    }

    $scope.login = function () {
        var data = getCredentials();

        if (!data) {
            return;
        }

        $http({
            method: 'POST',
            url: '/api/auth/login',
            data: JSON.stringify(data)
        }).then(function success(result) {
            $http.defaults.headers.common['x-auth'] = result.data;
            sessionStorage['authToken'] = result.data;
            sessionStorage['username'] = data.username;
            window.document.location = '/timeline';
        }, function error(error) {
            displayError(error);
        });
    };

    function getCredentials() {
        var credentials = {
            username: $scope.credentials.username,
            password: $scope.credentials.password
        };

        credentials.password = encryptPassword(credentials.password).toString();

        return credentials;
    }

    function encryptPassword(password) {
        return CryptoJS.SHA3(password);
    }

    function displayError(error) {
        $scope.error.message = error.data;
        $scope.error.visible = true
    }
});