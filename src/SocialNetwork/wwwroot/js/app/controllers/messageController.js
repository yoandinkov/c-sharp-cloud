﻿'use strict';

socialNetworkApp.controller('messageController', function ($scope, $http, $timeout) {
    $scope.content = '';
    $scope.successMessage = '';
    $scope.showSuccess = false;
    $scope.showError = false;
    $scope.errorMessage = '';
    $scope.messages = [];
    $scope.page = 0;
    $scope.count = "5";

    $scope.listMessages = function listMessages() {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/messages?page=' + $scope.page + '&count=' + $scope.count
        }).then(function (result) {
            $scope.messages = result.data;
        }, function (error) {
           showError(error);
        })
    };

    $scope.createMessage = function () {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'POST',
            data: {
                id: 0,
                content: $scope.content
            },
            url: '/api/messages'
        }).then(function (result) {
            $scope.content = '';

            displaySuccess('Message created');
        }, function (error) {
            displayError(error.data)
        })
    }

    $scope.noMessages = function () {
        if ($scope.messages) {
            return $scope.messages.length === 0;
        }

        return true;
    }

    $scope.isFirstPage = function () {
        return $scope.page == 0;
    }

    $scope.goToPrevPage = function () {
        if ($scope.page > 0) {
            $scope.page -= 1;
            $scope.listMessages();
        }
    }

    $scope.goToNextPage = function () {
        $scope.page += 1;
        $scope.listMessages();
    }

    $scope.isLastPage = function () {
        if ($scope.messages) {
            return $scope.messages.length === 0;
        }

        return true;
    }

    function displaySuccess(message) {
        $scope.showError = false;
        $scope.successMessage = message;
        $scope.showSuccess = true;

        $timeout(function () {
            $scope.showSuccess = false;
        }, 3000);
    }

    function displayError(message) {
        $scope.showSuccess = false;
        $scope.errorMessage = message;
        $scope.showError = true;
    }

    $scope.listMessages();
});