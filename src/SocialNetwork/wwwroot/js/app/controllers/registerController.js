﻿'use strict';

socialNetworkApp.controller('registerController', function ($scope, $http) {
    $scope.credentials = {
        username: '',
        description: '',
        email: '',
        password: '',
        secondPassword: ''
    };

    $scope.error = {
        visible: false,
        message: ''
    }

    $scope.register = function () {
        var data = getCredentials();

        if (!data) {
            return;
        }

        $http({
            method: 'POST',
            url: '/api/auth/register',
            data: JSON.stringify(data)
        }).then(function success(result) {
            sessionStorage['authToken'] = result.data;
            sessionStorage['username'] = data.username;
            window.document.location = '/timeline';
        }, function error (error) {
            displayError(error);
        });
    };

    function getCredentials() {
        if ($scope.credentials.password != $scope.credentials.secondPassword) {
            displayError("Password should match.");
            return false;
        }

        var credentials = {
            username: $scope.credentials.username,
            description: $scope.credentials.description,
            email: $scope.credentials.email,
            password: $scope.credentials.password
        };

        credentials.password = encryptPassword(credentials.password).toString();

        return credentials;
    }

    function encryptPassword(password) {
        if (!password) {
            return password;
        }

        return CryptoJS.SHA3(password);
    }

    function displayError(error) {
        $scope.error.message = error.data;
        $scope.error.visible = true
    }
});
