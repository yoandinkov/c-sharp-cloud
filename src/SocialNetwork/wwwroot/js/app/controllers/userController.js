﻿'use strict';

socialNetworkApp.controller('userController', function ($scope, $http) {
    $scope.page = 0;
    $scope.count = "25";

    $scope.listUsers = function () {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            method: 'GET',
            url: '/api/users?page=' + $scope.page + '&count=' + $scope.count
        }).then(function (result) {
            $scope.users = result.data;
        }, function (error) {
            console.error(error);
        });
    };

    $scope.isFirstPage = function () {
        return $scope.page == 0;
    }

    $scope.goToPrevPage = function () {
        if ($scope.page > 0) {
            $scope.page -= 1;
            $scope.listUsers();
        }
    }

    $scope.goToNextPage = function () {
        $scope.page += 1;
        $scope.listUsers();
    }

    $scope.isLastPage = function () {
        if ($scope.users) {
            return $scope.users.length === 0;
        }

        return true;
    }

    $scope.subscribeTo = function (user) {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"]
            },
            data: {
                userId: user.id
            },
            method: 'POST',
            url: '/api/subscriptions'
        }).then(function (result) {
            user.subscribedTo = true;
        }, function (error) {
            console.error(error);
        });
    }

    $scope.unsubscribeTo = function (user) {
        $http({
            headers: {
                'x-auth': sessionStorage["authToken"],
                "Content-Type": "application/json"
            },
            data: {
                userId: user.id
            },
            method: 'DELETE',
            url: '/api/subscriptions'
        }).then(function (result) {
            user.subscribedTo = false;
        }, function (error) {
            console.error(error);
        });
    }

    $scope.listUsers();
});