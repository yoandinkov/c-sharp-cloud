using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using SocialNetwork.Data;

namespace SocialNetwork.Migrations
{
    [DbContext(typeof(SocialNetworkDbContext))]
    partial class SocialNetworkDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348");

            modelBuilder.Entity("SocialNetwork.Models.Credential", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AuthToken");

                    b.Property<bool>("Deactivated");

                    b.Property<string>("Password");

                    b.Property<DateTime>("RegistrationDate");

                    b.Property<int>("UserId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SocialNetwork.Models.Message", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AuthorId");

                    b.Property<string>("Content");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateEdited");

                    b.Property<bool>("IsEdited");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SocialNetwork.Models.Subscription", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsBlocked");

                    b.Property<int>("SubscriberId");

                    b.Property<int>("SubscriptionAuthorId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SocialNetwork.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Email");

                    b.Property<string>("Username");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SocialNetwork.Models.Credential", b =>
                {
                    b.HasOne("SocialNetwork.Models.User")
                        .WithOne()
                        .HasForeignKey("SocialNetwork.Models.Credential", "UserId");
                });

            modelBuilder.Entity("SocialNetwork.Models.Message", b =>
                {
                    b.HasOne("SocialNetwork.Models.User")
                        .WithMany()
                        .HasForeignKey("AuthorId");
                });

            modelBuilder.Entity("SocialNetwork.Models.Subscription", b =>
                {
                    b.HasOne("SocialNetwork.Models.User")
                        .WithMany()
                        .HasForeignKey("SubscriberId");
                });
        }
    }
}
