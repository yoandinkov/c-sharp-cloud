using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace SocialNetwork.Migrations
{
    public partial class FixingMessageAuthorId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Credential_User_UserId", table: "Credential");
            migrationBuilder.DropForeignKey(name: "FK_Message_User_AuthorId1", table: "Message");
            migrationBuilder.DropForeignKey(name: "FK_Subscription_User_SubscriberId", table: "Subscription");
            migrationBuilder.DropColumn(name: "AuthorId1", table: "Message");
            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Message",
                nullable: false);
            migrationBuilder.AddForeignKey(
                name: "FK_Credential_User_UserId",
                table: "Credential",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Message_User_AuthorId",
                table: "Message",
                column: "AuthorId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subscription_User_SubscriberId",
                table: "Subscription",
                column: "SubscriberId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Credential_User_UserId", table: "Credential");
            migrationBuilder.DropForeignKey(name: "FK_Message_User_AuthorId", table: "Message");
            migrationBuilder.DropForeignKey(name: "FK_Subscription_User_SubscriberId", table: "Subscription");
            migrationBuilder.AlterColumn<Guid>(
                name: "AuthorId",
                table: "Message",
                nullable: false);
            migrationBuilder.AddColumn<int>(
                name: "AuthorId1",
                table: "Message",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_Credential_User_UserId",
                table: "Credential",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Message_User_AuthorId1",
                table: "Message",
                column: "AuthorId1",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subscription_User_SubscriberId",
                table: "Subscription",
                column: "SubscriberId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
