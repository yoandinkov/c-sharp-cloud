﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using SocialNetwork.Data;
using SocialNetwork.Filters;
using SocialNetwork.Helpers;
using SocialNetwork.Models;
using SocialNetwork.ViewModels;

namespace SocialNetwork.Api
{
    [Authentication]
    public class MessagesController : BaseController
    {
        private readonly ISocialNetworkDbContext _dbContext;

        public MessagesController(ISocialNetworkDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("/api/[controller]/my")]
        public IActionResult Mine(int count = 10, int page = 0)
        {
            var messages =
                CurrentUser
                .Messages
                .Paginate(count, page)
                .OrderBy(x => x.DateEdited ?? x.DateCreated).ToList();

            var result = ToViewModel(messages);

            return Ok(result);
        }

        [HttpGet]
        [ModelValidation]
        [Route("/api/[controller]")]
        public IActionResult Newest(int count = 10, int page = 0)
        {
            var messages = NewestMessages(count, page).ToList();

            var result = ToViewModel(messages);

            return Ok(result);
        }

        [HttpGet]
        [Route("/api/[controller]/by-user")]
        public IActionResult NewestByUser(string username, int count = 10, int page = 0)
        {
            var messages = NewestMessages(count, page).ToList().Where(x => x.Author.Username == username).ToList();

            var result = ToViewModel(messages);

            return Ok(result);
        }

        [HttpPost]
        [ModelValidation]
        [Route("/api/[controller]")]
        public IActionResult Create([FromBody]MessageCreateViewModel model)
        {
            var message = new Message
            {
                AuthorId = CurrentUser.Id,
                Content = model.Content,
                DateCreated = DateTime.Now
            };

            _dbContext.Messages.Add(message);

            _dbContext.SaveChanges();

            return new CreatedResult("/api/messages", model);
        }

        [HttpPut]
        [ModelValidation]
        [Route("/api/[controller]")]
        public IActionResult Edit([FromBody]MessageCreateViewModel model)
        {
            var message = CurrentUser.Messages.FirstOrDefault(x => x.Id == model.MessageId);

            if (message == null)
            {
                return new HttpNotFoundObjectResult("Message with such id doesn't exists");
            }

            message.Content = model.Content;
            message.DateEdited = DateTime.Now;
            message.IsEdited = true;

            _dbContext.SaveChanges();

            return new EmptyResult();
        }

        [HttpDelete]
        [ModelValidation]
        [Route("/api/[controller]")]
        public IActionResult Remove([FromBody]MessageRemoveViewModel model)
        {
            var message = CurrentUser.Messages.FirstOrDefault(x => x.Id == model.MessageId);

            if (message == null)
            {
                return HttpBadRequest();
            }

            _dbContext.Messages.Remove(message);

            _dbContext.SaveChanges();

            return new EmptyResult();
        }

        private IQueryable<Message> NewestMessages(int count, int page)
        {
            var authors = CurrentUser.Subscriptions.Where(x => x.IsBlocked == false).Select(x => x.SubscriptionAuthorId).ToList();

            var messages =
                _dbContext
                .Messages
                .Include(x => x.Author)
                .Paginate(count, page)
                .Where(x => authors.Contains(x.AuthorId))
                .OrderBy(x => x.DateEdited ?? x.DateCreated);

            return messages;
        }

        private List<MessageViewModel> ToViewModel(List<Message> messages)
        {
            return messages.Select(message => new MessageViewModel
            {
                Id = message.Id,
                Author = message.Author.Username,
                Content = message.Content,
                DateCreated = message.DateCreated,
                DateEdited = message.DateEdited,
                IsEdited = message.IsEdited
            }).ToList();

        }
    }
}
