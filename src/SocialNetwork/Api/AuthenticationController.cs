﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using SocialNetwork.Data;
using SocialNetwork.Filters;
using SocialNetwork.Helpers;
using SocialNetwork.Models;
using SocialNetwork.ViewModels;

namespace SocialNetwork.Api
{
    [Authentication]
    [Route("/api/auth/[action]")]
    public class AuthenticationController : BaseController
    {
        private readonly int AUTH_TOKEN_LENGTH = 50;

        private readonly ISocialNetworkDbContext _dbContext;

        public AuthenticationController(ISocialNetworkDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginViewModel model)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Username == model.Username);

            if (user == null)
            {
                return HttpBadRequest("User/Password doesn't match");
            }

            var credential = await _dbContext.Credentials.SingleAsync(x => x.UserId == user.Id);

            if (SecurityHelper.CheckPassword(credential.Password, model.Password) == false)
            {
                return HttpBadRequest("User/Password doesn't match");
            }

            credential.AuthToken = SecurityHelper.GenerateIdentifier(AUTH_TOKEN_LENGTH);

            _dbContext.SaveChanges();

            return Ok(credential.AuthToken);
        }

        [HttpPost]
        [ModelValidation]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel model)
        {
            User existing = await _dbContext.Users.FirstOrDefaultAsync(x => x.Username == model.Username);
            
            if (existing != null)
            {
                return HttpBadRequest("User with such already exists.");
            }

            var user = new User
            {
                Description = model.Description,
                Email = model.Email,
                Username = model.Username
            };

            _dbContext.Users.Add(user);

            var credential = new Credential
            {
                AuthToken = SecurityHelper.GenerateIdentifier(AUTH_TOKEN_LENGTH),
                Deactivated = false,
                UserId = user.Id,
                Password = SecurityHelper.HashPassword(model.Password),
                RegistrationDate = DateTime.Now
            };

            _dbContext.Credentials.Add(credential);

            _dbContext.SaveChanges();

            return Ok(credential.AuthToken);
        }

        [HttpPut]
        public IActionResult Logout()
        {
            CurrentUser.Credential.AuthToken = null;

            _dbContext.SaveChanges();

            return new NoContentResult();
        }
    }
}
