﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using SocialNetwork.Data;
using SocialNetwork.Filters;
using SocialNetwork.Helpers;
using SocialNetwork.ViewModels;

namespace SocialNetwork.Api
{
    [Authentication]
    public class UsersController : BaseController
    {
        private readonly ISocialNetworkDbContext _dbContext;

        public UsersController(ISocialNetworkDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("api/[controller]")]
        public IActionResult All(int count = 10, int page = 0)
        {
            var hidden = CurrentUser.Subscriptions.Where(x => x.IsBlocked == true).Select(x => x.SubscriptionAuthorId).ToList();

            var users = _dbContext
                .Users
                .Where(x => x.Id != CurrentUser.Id && hidden.Contains(x.Id) == false)
                .OrderBy(x => x.Username)
                .Paginate(count, page).Select(u => new UserViewModel
                {
                    Id = u.Id,
                    Description = u.Description,
                    Username = u.Username,
                    SubscribedTo = CurrentUser.Subscriptions.Any(s => s.SubscriptionAuthorId == u.Id)
                });

            return Ok(users);
        }

        [HttpPut]
        [ModelValidation]
        [Route("api/[controller]")]
        public IActionResult Edit([FromBody]EditProfileViewModel model)
        {
            CurrentUser.Description = model.Description;

            if (string.IsNullOrEmpty(model.NewPassword) == false)
            {
                var isPasswordValid = SecurityHelper.CheckPassword(CurrentUser.Credential.Password, model.CurrentPassword);

                if (string.IsNullOrEmpty(model.NewPassword) == false && isPasswordValid)
                {
                    CurrentUser.Credential.Password = SecurityHelper.HashPassword(model.NewPassword);
                }
            }

            _dbContext.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("api/[controller]/personal-info")]
        public IActionResult PersonInformation()
        {
            var information = new UserInformationViewModel
            {
                Description = CurrentUser.Description,
                Email = CurrentUser.Email,
                Username = CurrentUser.Username
            };

            return Ok(information);
        }
    }
}
