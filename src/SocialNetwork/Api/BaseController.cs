﻿using Microsoft.AspNet.Mvc;
using SocialNetwork.Models;

namespace SocialNetwork.Api
{
    public class BaseController : Controller
    {
        public User CurrentUser { get; set; }
    }
}
