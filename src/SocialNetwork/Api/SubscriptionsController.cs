﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using SocialNetwork.Data;
using SocialNetwork.Filters;
using SocialNetwork.Helpers;
using SocialNetwork.Models;
using SocialNetwork.ViewModels;
using SocialNetwork.ViewModels.Subscription;

namespace SocialNetwork.Api
{
    [Authentication]
    public class SubscriptionsController : BaseController
    {
        private readonly ISocialNetworkDbContext _dbContext;

        public SubscriptionsController(ISocialNetworkDbContext dbContext)
        {
            _dbContext = dbContext;

        }

        [HttpGet]
        [Route("/api/[controller]")]
        public IActionResult MySubscriptions(int count = 10, int page = 0)
        {
            var authorIds = CurrentUser.Subscriptions
                .Select(x => x.SubscriptionAuthorId)
                .Paginate(count, page)
                .ToList();

            var authors = _dbContext.Users.Where(x => authorIds.Contains(x.Id)).ToList();

            var result = authors.Select(x => new SubscriptionViewModel
            {
                Author = x.Username,
                AuthorId = x.Id
            });

            result.OrderBy(x => x.Author);
            
            return Ok(result);
        }

        [HttpPost]
        [Route("/api/[controller]")]
        public IActionResult Subscribe([FromBody]SubscriptionEditViewModel model)
        {
            var subscriptionAuthor = _dbContext.Users.FirstOrDefault(x => x.Id == model.UserId);

            if (subscriptionAuthor == null)
            {
                return HttpBadRequest();
            }

            var subscription = new Subscription
            {
                IsBlocked = false,
                SubscriberId = CurrentUser.Id,
                SubscriptionAuthorId = subscriptionAuthor.Id
            };

            _dbContext.Subscriptions.Add(subscription);

            _dbContext.SaveChanges();

            return Created("/api/subscriptions/", model);
        }

        [HttpDelete]
        [Route("/api/[controller]")]
        public IActionResult Unsubscribe([FromBody]SubscriptionEditViewModel model)
        {
            var subscription = CurrentUser.Subscriptions.FirstOrDefault(x => x.SubscriptionAuthorId == model.UserId);

            if (subscription == null)
            {
                return HttpBadRequest();
            }

            _dbContext.Subscriptions.Remove(subscription);

            _dbContext.SaveChanges();

            return new EmptyResult();
        }

        [HttpGet]
        [Route("api/subscribers")]
        public IActionResult MySubscribers(int count = 10, int page = 0)
        {
            var subscribers = _dbContext.Subscriptions.Include(x => x.Subscriber).Where(x => x.SubscriptionAuthorId == CurrentUser.Id).Paginate(count, page).ToList();

            var result = subscribers.Select(x => new SubscriberViewModel()
            {
                SubscriptionId = x.Id,
                Username = x.Subscriber.Username,
                IsBlocked = x.IsBlocked
            });

            return Ok(result);
        }

        [HttpPut]
        [ModelValidation]
        [Route("api/subscribers/block")]
        public IActionResult BlockSubscriber([FromBody]EditSubscriberViewModel model)
        {
            var subscription = _dbContext.Subscriptions.FirstOrDefault(x => x.Id == model.SubscriptionId);

            if (subscription == null)
            {
                return HttpNotFound("Subscription wich such id is not found");
            }

            if (subscription.SubscriptionAuthorId != CurrentUser.Id)
            {
                return HttpUnauthorized();
            }

            subscription.IsBlocked = true;

            _dbContext.SaveChanges();

            return new EmptyResult();
        }

        [HttpPut]
        [ModelValidation]
        [Route("api/subscribers/unblock")]
        public IActionResult UnblockSubscriber([FromBody]EditSubscriberViewModel model)
        {
            var subscription = _dbContext.Subscriptions.FirstOrDefault(x => x.Id == model.SubscriptionId);

            if (subscription == null)
            {
                return HttpBadRequest();
            }

            if (subscription.SubscriptionAuthorId != CurrentUser.Id)
            {
                return HttpBadRequest();
            }

            subscription.IsBlocked = false;

            _dbContext.SaveChanges();

            return new EmptyResult();
        }
    }
}
