﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;

namespace SocialNetwork.Filters
{
    public class ModelValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var allErrors = actionContext.ModelState.Values.SelectMany(v => v.Errors);

                var errorMessage = string.Join("\n", allErrors.Select(x => x.ErrorMessage));

                actionContext.Result = new BadRequestObjectResult(errorMessage);
            }
        }
    }
}
