﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;
using Microsoft.Data.Entity;
using SocialNetwork.Api;
using SocialNetwork.Data;

namespace SocialNetwork.Filters
{
    public class AuthenticationAttribute : ActionFilterAttribute
    {
        private ISocialNetworkDbContext _dbContext;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _dbContext = (ISocialNetworkDbContext)((BaseController)context.Controller).Resolver.GetService(typeof(ISocialNetworkDbContext));

            if (context.Filters.Any(x => x.GetType() == typeof(AllowAnonymousFilter)) == false)
            {
                string authToken = context.HttpContext.Request.Headers["x-auth"];

                var credential = _dbContext.Credentials.Include(x => x.User).FirstOrDefault(c => c.AuthToken == authToken);

                if (credential == null)
                {
                    context.Result = new HttpUnauthorizedResult();
                }
                else
                {
                    ((BaseController)context.Controller).CurrentUser = _dbContext
                        .Users
                        .Include(x => x.Credential)
                        .Include(x => x.Messages)
                        .Include(x => x.Subscriptions)
                        .FirstOrDefault(x => x.Id == credential.UserId);
                }
            }
        }
    }
}
