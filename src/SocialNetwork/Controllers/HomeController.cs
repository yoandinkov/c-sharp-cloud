﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace SocialNetwork.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
