﻿using System;
using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class MessageRemoveViewModel
    {
        [JsonProperty("messageId")]
        public int MessageId { get; set; }
    }
}
