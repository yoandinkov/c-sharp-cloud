﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class MessageCreateViewModel
    {
        [JsonProperty("id")]
        public int MessageId { get; set; }
        
        [Required]
        [StringLength(140, MinimumLength = 3, ErrorMessage = "{0} should be between {2} and {1} symbols long.")]
        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
