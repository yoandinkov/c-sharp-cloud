﻿using System;
using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class MessageViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("isEdited")]
        public bool IsEdited { get; set; }

        [JsonProperty("dateCreated")]
        public DateTime DateCreated { get; set; }

        [JsonProperty("dateEdited")]
        public DateTime? DateEdited { get; set; }

        [JsonProperty("author")]
        public string Author { get; set; }
    }
}
