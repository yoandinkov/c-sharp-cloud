﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class EditProfileViewModel
    {
        [JsonProperty("description")]
        [Required]
        [StringLength(140, MinimumLength = 10, ErrorMessage = "{0} should be between {2} and {1} symbols long.")]
        public string Description { get; set; }

        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }

        [JsonProperty("oldPassword")]
        public string CurrentPassword { get; set; }
    }
}
