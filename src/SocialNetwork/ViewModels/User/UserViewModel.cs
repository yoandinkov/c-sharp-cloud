﻿using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class UserViewModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("subscribedTo")]
        public bool SubscribedTo { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
