﻿using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class UserInformationViewModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
