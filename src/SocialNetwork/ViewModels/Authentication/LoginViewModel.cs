﻿using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class LoginViewModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
