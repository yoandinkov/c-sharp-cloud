﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class RegisterViewModel
    {
        [JsonProperty("username")]
        [Required]
        [StringLength(18, MinimumLength = 3, ErrorMessage = "{0} should be between {2} and {1} symbols long.")]
        public string Username { get; set; }

        [JsonProperty("email")]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [JsonProperty("description")]
        [StringLength(140, MinimumLength = 10, ErrorMessage = "{0} should be between {2} and {1} symbols long.")]
        public string Description { get; set; }

        [JsonProperty("password")]
        [Required]
        public string Password { get; set; }
    }
}
