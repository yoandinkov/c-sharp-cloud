﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class SubscriptionEditViewModel
    {
        [Required]
        [JsonProperty("userId")]
        public int UserId { get; set; }
    }
}
