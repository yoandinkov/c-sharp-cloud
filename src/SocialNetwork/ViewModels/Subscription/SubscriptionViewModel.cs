﻿using Newtonsoft.Json;

namespace SocialNetwork.ViewModels.Subscription
{
    public class SubscriptionViewModel
    {
        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }
    }
}
