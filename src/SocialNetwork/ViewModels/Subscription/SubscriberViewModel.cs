﻿using Newtonsoft.Json;

namespace SocialNetwork.ViewModels
{
    public class SubscriberViewModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("isBlocked")]
        public bool IsBlocked { get; set; }

        [JsonProperty("subscriptionId")]
        public int SubscriptionId { get; internal set; }
    }
}
