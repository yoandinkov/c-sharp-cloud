﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SocialNetwork.Data;

namespace SocialNetwork
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            Configuration["Data:DefaultConnection:ConnectionString"] = $@"Data Source=.\SocialNetwork.sqlite";
        }

        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();


            services.AddEntityFramework()
              .AddSqlite()
              .AddDbContext<SocialNetworkDbContext>(options => options.UseSqlite(Configuration["Data:DefaultConnection:ConnectionString"]));

            services.AddScoped<ISocialNetworkDbContext>(provider => provider.GetService<SocialNetworkDbContext>());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseIISPlatformHandler();

            app.UseStaticFiles();

            app.UseDeveloperExceptionPage();

            try
            {
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                    .CreateScope())
                {
                    serviceScope.ServiceProvider.GetService<SocialNetworkDbContext>()
                         .Database.Migrate();
                }
            }
            catch { }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                   name: "api",
                   template: "api/{controller}/{action}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{*.}",
                    defaults: new { controller = "Home", action="Index" });

            });
        }
        
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
