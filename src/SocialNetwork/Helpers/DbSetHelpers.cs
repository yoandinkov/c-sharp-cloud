﻿using System.Collections.Generic;
using System.Linq;

namespace SocialNetwork.Helpers
{
    public static class DbSetHelpers
    {
        public static IQueryable<TSource> Paginate<TSource>(this IQueryable<TSource> source, int count, int page)
        {
            return source.Skip(page * count).Take(count);
        }

        public static IEnumerable<TSource> Paginate<TSource>(this IEnumerable<TSource> source, int count, int page)
        {
            return source.Skip(page * count).Take(count);
        }
    }
}
