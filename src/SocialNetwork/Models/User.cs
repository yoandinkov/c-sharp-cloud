﻿using System;
using System.Collections.Generic;

namespace SocialNetwork.Models
{
    public class User
    {
        public User()
        {
            Messages = new HashSet<Message>();
            Subscriptions = new HashSet<Subscription>();
        }

        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<Subscription> Subscriptions { get; set; }

        public virtual Credential Credential { get; set; }
    }
}
