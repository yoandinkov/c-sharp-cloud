﻿using System;

namespace SocialNetwork.Models
{
    public class Message
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public bool IsEdited { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateEdited { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }
    }
}