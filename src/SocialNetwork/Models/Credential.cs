﻿using System;

namespace SocialNetwork.Models
{
    public class Credential
    {
        public int Id { get; set; }

        public string AuthToken { get; set; }

        public string Password { get; set; }

        public bool Deactivated { get; set; }

        public DateTime RegistrationDate { get; set; }

        public virtual User User { get; set; }

        public int UserId { get; set; }
    }
}
