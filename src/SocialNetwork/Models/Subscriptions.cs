﻿using System;

namespace SocialNetwork.Models
{
    public class Subscription
    {
        public int Id { get; set; }

        public int SubscriberId { get; set; }

        public virtual User Subscriber { get; set; }

        public int SubscriptionAuthorId { get; set; }

        public bool IsBlocked { get; set; }
    }
}