﻿using SocialNetwork.Models;
using Microsoft.Data.Entity;

namespace SocialNetwork.Data
{
    public class SocialNetworkDbContext : DbContext, ISocialNetworkDbContext
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<Credential> Credentials { get; set; }
    }
}
