﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using SocialNetwork.Models;

namespace SocialNetwork.Data
{
    public interface ISocialNetworkDbContext
    {
        DbSet<User> Users { get; set; }

        DbSet<Message> Messages { get; set; }

        DbSet<Subscription> Subscriptions { get; set; }

        DbSet<Credential> Credentials { get; set; }

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
